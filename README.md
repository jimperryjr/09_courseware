# Courseware Elements #
http://13.68.209.70/09_courseware/index.htm

(*right click and open the above link in a new tab or window*)

These are links to elements that I have programmed
for a proprietary courseware development tool.

**Background:** At the time, the tool did not have
Capabilities such as Storyline, Captivate etc.
So interactions such as knowledge checks,
check on learnings, practical applications 
or exercises had to be created from scratch.

Usually team members would create the 3D 
or graphics and I would program the logic and 
tie everything together. I would also tie it into
the backend of the proprietary tool and SCORM API.

Courseware developers would take the final 
programmed asset and insert it into a lesson.
If resources were limited, I would create the 
3D assets, use VBS2 or crazy talk as elements
while programming the logic of the simulated 
scenarios. Afterwards I would implement the 
completed piece in the lesson.